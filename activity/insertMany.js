db.rooms.insertMany(
    [
    {
        name: "double",
        accommodates: "3",
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        rooms_available: "5",
        isAvailable: false
    },
    {
        name: "queen",
        accommodates: "4",
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getawa",
        rooms_available: "15",
        isAvailable: false
        }
        ]
        )